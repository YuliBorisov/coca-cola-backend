'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">jwttest documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/ActivityModule.html" data-type="entity-link">ActivityModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-ActivityModule-fa59fc92263fe20f04f2780e7762d7d2"' : 'data-target="#xs-controllers-links-module-ActivityModule-fa59fc92263fe20f04f2780e7762d7d2"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-ActivityModule-fa59fc92263fe20f04f2780e7762d7d2"' :
                                            'id="xs-controllers-links-module-ActivityModule-fa59fc92263fe20f04f2780e7762d7d2"' }>
                                            <li class="link">
                                                <a href="controllers/ActivityController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ActivityController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ActivityModule-fa59fc92263fe20f04f2780e7762d7d2"' : 'data-target="#xs-injectables-links-module-ActivityModule-fa59fc92263fe20f04f2780e7762d7d2"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ActivityModule-fa59fc92263fe20f04f2780e7762d7d2"' :
                                        'id="xs-injectables-links-module-ActivityModule-fa59fc92263fe20f04f2780e7762d7d2"' }>
                                        <li class="link">
                                            <a href="injectables/ActivityService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ActivityService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-d9c04f19999351dfe1efdc55be9ea414"' : 'data-target="#xs-controllers-links-module-AppModule-d9c04f19999351dfe1efdc55be9ea414"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-d9c04f19999351dfe1efdc55be9ea414"' :
                                            'id="xs-controllers-links-module-AppModule-d9c04f19999351dfe1efdc55be9ea414"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AuthModule-69dddd642003e32386ebcf932b8393d0"' : 'data-target="#xs-controllers-links-module-AuthModule-69dddd642003e32386ebcf932b8393d0"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AuthModule-69dddd642003e32386ebcf932b8393d0"' :
                                            'id="xs-controllers-links-module-AuthModule-69dddd642003e32386ebcf932b8393d0"' }>
                                            <li class="link">
                                                <a href="controllers/AuthController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthModule-69dddd642003e32386ebcf932b8393d0"' : 'data-target="#xs-injectables-links-module-AuthModule-69dddd642003e32386ebcf932b8393d0"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-69dddd642003e32386ebcf932b8393d0"' :
                                        'id="xs-injectables-links-module-AuthModule-69dddd642003e32386ebcf932b8393d0"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JwtStrategy.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>JwtStrategy</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/BarcodeSubmisionModule.html" data-type="entity-link">BarcodeSubmisionModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-BarcodeSubmisionModule-d31c41334eb178e984e331c899b39d66"' : 'data-target="#xs-controllers-links-module-BarcodeSubmisionModule-d31c41334eb178e984e331c899b39d66"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-BarcodeSubmisionModule-d31c41334eb178e984e331c899b39d66"' :
                                            'id="xs-controllers-links-module-BarcodeSubmisionModule-d31c41334eb178e984e331c899b39d66"' }>
                                            <li class="link">
                                                <a href="controllers/BarcodeSubmisionController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BarcodeSubmisionController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-BarcodeSubmisionModule-d31c41334eb178e984e331c899b39d66"' : 'data-target="#xs-injectables-links-module-BarcodeSubmisionModule-d31c41334eb178e984e331c899b39d66"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-BarcodeSubmisionModule-d31c41334eb178e984e331c899b39d66"' :
                                        'id="xs-injectables-links-module-BarcodeSubmisionModule-d31c41334eb178e984e331c899b39d66"' }>
                                        <li class="link">
                                            <a href="injectables/BarcodeSubmisionService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>BarcodeSubmisionService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConfigModule.html" data-type="entity-link">ConfigModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CoreModule-bbcb85d34a499d9eae1059c82431c2fa"' : 'data-target="#xs-injectables-links-module-CoreModule-bbcb85d34a499d9eae1059c82431c2fa"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CoreModule-bbcb85d34a499d9eae1059c82431c2fa"' :
                                        'id="xs-injectables-links-module-CoreModule-bbcb85d34a499d9eae1059c82431c2fa"' }>
                                        <li class="link">
                                            <a href="injectables/MailerService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MailerService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MappingService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MappingService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CustomerModule.html" data-type="entity-link">CustomerModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-CustomerModule-9533c861a8b8cdb9e290f88b9b020454"' : 'data-target="#xs-controllers-links-module-CustomerModule-9533c861a8b8cdb9e290f88b9b020454"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-CustomerModule-9533c861a8b8cdb9e290f88b9b020454"' :
                                            'id="xs-controllers-links-module-CustomerModule-9533c861a8b8cdb9e290f88b9b020454"' }>
                                            <li class="link">
                                                <a href="controllers/CustomerController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CustomerController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CustomerModule-9533c861a8b8cdb9e290f88b9b020454"' : 'data-target="#xs-injectables-links-module-CustomerModule-9533c861a8b8cdb9e290f88b9b020454"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CustomerModule-9533c861a8b8cdb9e290f88b9b020454"' :
                                        'id="xs-injectables-links-module-CustomerModule-9533c861a8b8cdb9e290f88b9b020454"' }>
                                        <li class="link">
                                            <a href="injectables/CustomerService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>CustomerService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MappingService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MappingService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/OutletService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>OutletService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/OutletModule.html" data-type="entity-link">OutletModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-OutletModule-ac1baa3eaff239f40d3fe101247bc56e"' : 'data-target="#xs-controllers-links-module-OutletModule-ac1baa3eaff239f40d3fe101247bc56e"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-OutletModule-ac1baa3eaff239f40d3fe101247bc56e"' :
                                            'id="xs-controllers-links-module-OutletModule-ac1baa3eaff239f40d3fe101247bc56e"' }>
                                            <li class="link">
                                                <a href="controllers/OutletController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OutletController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-OutletModule-ac1baa3eaff239f40d3fe101247bc56e"' : 'data-target="#xs-injectables-links-module-OutletModule-ac1baa3eaff239f40d3fe101247bc56e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-OutletModule-ac1baa3eaff239f40d3fe101247bc56e"' :
                                        'id="xs-injectables-links-module-OutletModule-ac1baa3eaff239f40d3fe101247bc56e"' }>
                                        <li class="link">
                                            <a href="injectables/MappingService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MappingService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/OutletService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>OutletService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/RedemptionReportModule.html" data-type="entity-link">RedemptionReportModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-RedemptionReportModule-2563108aea4b7fe4121d74a7eb484c25"' : 'data-target="#xs-controllers-links-module-RedemptionReportModule-2563108aea4b7fe4121d74a7eb484c25"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-RedemptionReportModule-2563108aea4b7fe4121d74a7eb484c25"' :
                                            'id="xs-controllers-links-module-RedemptionReportModule-2563108aea4b7fe4121d74a7eb484c25"' }>
                                            <li class="link">
                                                <a href="controllers/RedemptionReportController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RedemptionReportController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-RedemptionReportModule-2563108aea4b7fe4121d74a7eb484c25"' : 'data-target="#xs-injectables-links-module-RedemptionReportModule-2563108aea4b7fe4121d74a7eb484c25"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-RedemptionReportModule-2563108aea4b7fe4121d74a7eb484c25"' :
                                        'id="xs-injectables-links-module-RedemptionReportModule-2563108aea4b7fe4121d74a7eb484c25"' }>
                                        <li class="link">
                                            <a href="injectables/MappingService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MappingService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/RedemptionReportService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>RedemptionReportService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link">UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UserModule-efe9d4e19b67264a2969f3a6d3b95d65"' : 'data-target="#xs-controllers-links-module-UserModule-efe9d4e19b67264a2969f3a6d3b95d65"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-efe9d4e19b67264a2969f3a6d3b95d65"' :
                                            'id="xs-controllers-links-module-UserModule-efe9d4e19b67264a2969f3a6d3b95d65"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-efe9d4e19b67264a2969f3a6d3b95d65"' : 'data-target="#xs-injectables-links-module-UserModule-efe9d4e19b67264a2969f3a6d3b95d65"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-efe9d4e19b67264a2969f3a6d3b95d65"' :
                                        'id="xs-injectables-links-module-UserModule-efe9d4e19b67264a2969f3a6d3b95d65"' }>
                                        <li class="link">
                                            <a href="injectables/MappingService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MappingService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/ActivityController.html" data-type="entity-link">ActivityController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link">AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/AuthController.html" data-type="entity-link">AuthController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/BarcodeSubmisionController.html" data-type="entity-link">BarcodeSubmisionController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/CustomerController.html" data-type="entity-link">CustomerController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/OutletController.html" data-type="entity-link">OutletController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/RedemptionReportController.html" data-type="entity-link">RedemptionReportController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link">UserController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Activity.html" data-type="entity-link">Activity</a>
                            </li>
                            <li class="link">
                                <a href="classes/AdminRegisterDTO.html" data-type="entity-link">AdminRegisterDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/Barcode.html" data-type="entity-link">Barcode</a>
                            </li>
                            <li class="link">
                                <a href="classes/BarcodeDTO.html" data-type="entity-link">BarcodeDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateCustomerDTO.html" data-type="entity-link">CreateCustomerDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateOutletDTO.html" data-type="entity-link">CreateOutletDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/Customer.html" data-type="entity-link">Customer</a>
                            </li>
                            <li class="link">
                                <a href="classes/Outlet.html" data-type="entity-link">Outlet</a>
                            </li>
                            <li class="link">
                                <a href="classes/Prize.html" data-type="entity-link">Prize</a>
                            </li>
                            <li class="link">
                                <a href="classes/PrizeBarcodeDTO.html" data-type="entity-link">PrizeBarcodeDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/RedemptionRecord.html" data-type="entity-link">RedemptionRecord</a>
                            </li>
                            <li class="link">
                                <a href="classes/Role.html" data-type="entity-link">Role</a>
                            </li>
                            <li class="link">
                                <a href="classes/ShowBasicUserDTO.html" data-type="entity-link">ShowBasicUserDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ShowCustomerDTO.html" data-type="entity-link">ShowCustomerDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ShowOutletDTO.html" data-type="entity-link">ShowOutletDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ShowRedemptionRecordDTO.html" data-type="entity-link">ShowRedemptionRecordDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ShowUserDTO.html" data-type="entity-link">ShowUserDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link">User</a>
                            </li>
                            <li class="link">
                                <a href="classes/userFix1563011733654.html" data-type="entity-link">userFix1563011733654</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserLoginDTO.html" data-type="entity-link">UserLoginDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserMappingDTO.html" data-type="entity-link">UserMappingDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserRegisterDTO.html" data-type="entity-link">UserRegisterDTO</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ActivityService.html" data-type="entity-link">ActivityService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BarcodeSubmisionService.html" data-type="entity-link">BarcodeSubmisionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfigService.html" data-type="entity-link">ConfigService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CustomerService.html" data-type="entity-link">CustomerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtStrategy.html" data-type="entity-link">JwtStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MailerService.html" data-type="entity-link">MailerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MappingService.html" data-type="entity-link">MappingService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OutletService.html" data-type="entity-link">OutletService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RedemptionReportService.html" data-type="entity-link">RedemptionReportService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link">UserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsersService.html" data-type="entity-link">UsersService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/ControllerRolesGuard.html" data-type="entity-link">ControllerRolesGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/RolesGuard.html" data-type="entity-link">RolesGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/EnvConfig.html" data-type="entity-link">EnvConfig</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/JwtPayload.html" data-type="entity-link">JwtPayload</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});