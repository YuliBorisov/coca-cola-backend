import { Injectable } from '@nestjs/common';
import { Customer } from '../../data/entities/customer';
import { Outlet } from '../../data/entities/outlet';
import { RedemptionRecord } from '../../data/entities/redemptionRecord';
import { User } from '../../data/entities/user';
import { ShowCustomerDTO } from '../../models/customer/show-customer-dto';
import { ShowOutletDTO } from '../../models/outlet/show-outlet-dto';
import { ShowRedemptionRecordDTO } from '../../models/redemption-record/redemption-record-dto';
import { ShowBasicUserDTO } from '../../models/user/show-basicuser-dto';
import { ShowUserDTO } from '../../models/user/show-user-dto';

@Injectable()
export class MappingService {
  async returnCustomer(customer: Customer): Promise<ShowCustomerDTO> {
    return {
      id: customer.id,
      brand: customer.brandName,
      createdOn: customer.createdOn,
    };
  }

  async returnOutlet(outlet: Outlet): Promise<ShowOutletDTO> {
    return {
      id: outlet.id,
      location: outlet.location,
      createdOn: outlet.createdOn,
    };
  }

  async returnBasicUser(user: User): Promise<ShowBasicUserDTO> {
    const userBrand: Customer = (user as any).__brand__;
    const userOutlet: Outlet = (user as any).__outlet__;

    return {
      id: user.id,
      userName: user.name,
      createdOn: user.createdOn,
      brandName: userBrand,
      outletName: userOutlet,
    };
  }

  async returnUser(user: User): Promise<ShowUserDTO> {
    return {
      id: user.id,
      userName: user.name,
      createdOn: user.createdOn,
    };
  }

  async returnRedemptionRecord(
    redemptionRecord: RedemptionRecord,
  ): Promise<ShowRedemptionRecordDTO> {
    const recordBrand: Customer = (redemptionRecord as any).__brand__;
    const recordOutlet: Outlet = (redemptionRecord as any).__outlet__;
    const recordUser: User = (redemptionRecord as any).__user__;
    return {
      id: redemptionRecord.id,
      barcode: redemptionRecord.barcode.id,
      brand: {
        id: recordBrand.id,
        brand: recordBrand.brandName,
        createdOn: recordBrand.createdOn,
      },
      outlet: {
        id: recordOutlet.id,
        location: recordOutlet.location,
        createdOn: recordOutlet.createdOn,
      },
      prize: redemptionRecord.prize,
      user: {
        id: recordUser.id,
        userName: recordUser.name,
        createdOn: recordUser.createdOn,
      },
      timestamp: redemptionRecord.createdOn,
    };
  }
}
