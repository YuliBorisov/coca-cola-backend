import { IsString, Matches, Length } from 'class-validator';

export class BarcodeDTO {
  @IsString()
  @Matches(/^[a-zA-Z0-9]*$/)
  @Length(13)
  barcode: string;
}
