import { IsString } from 'class-validator';

export class ShowCustomerDTO {

  @IsString()
  id: string;

  @IsString()
  brand: string;

  @IsString()
  createdOn: Date;
}
