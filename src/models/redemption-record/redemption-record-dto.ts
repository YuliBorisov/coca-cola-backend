import { IsDate, IsString, Length } from 'class-validator';
import { Prize } from '../../data/entities/prize';
import { ShowCustomerDTO } from '../customer/show-customer-dto';
import { ShowOutletDTO } from '../outlet/show-outlet-dto';
import { ShowUserDTO } from '../user/show-user-dto';

export class ShowRedemptionRecordDTO {
  @IsString()
  id: string;

  @IsString()
  @Length(13)
  barcode: string;

  brand: ShowCustomerDTO;

  outlet: ShowOutletDTO;

  prize: Prize;

  user: ShowUserDTO;

  @IsDate()
  timestamp: Date;
}
