import { IsString } from 'class-validator';

export class ShowUserDTO {

  @IsString()
  id: string;

  @IsString()
  userName: string;

  @IsString()
  createdOn: Date;

}
