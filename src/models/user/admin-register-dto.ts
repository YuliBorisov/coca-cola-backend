import { IsEmail, IsString, Matches, Length } from 'class-validator';

export class AdminRegisterDTO {

  @IsString()
  @Length(3, 10)
  name: string;

  @IsEmail()
  email: string;

  @IsString()
  @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  password: string;

}
