import { IsString } from 'class-validator';

export class UserMappingDTO {

  @IsString()
  outletId: string;

}
