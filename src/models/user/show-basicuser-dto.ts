import { IsString } from 'class-validator';

export class ShowBasicUserDTO {

  @IsString()
  id: string;

  @IsString()
  userName: string;

  @IsString()
  createdOn: Date;

  @IsString()
  brandName: any;

  @IsString()
  outletName: any;

}
