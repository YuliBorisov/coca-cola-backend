import { IsString } from 'class-validator';

export class ShowOutletDTO {

  @IsString()
  id: string;

  @IsString()
  location: string;

  @IsString()
  createdOn: Date;

}
