import { IsString, Length } from 'class-validator';

export class CreateOutletDTO {
  @IsString()
  @Length(3)
  location: string;
}
