export enum ActivityType {
  CREATED = 'created',
  DELETED = 'deleted',
  UPDATED = 'updated',
}
