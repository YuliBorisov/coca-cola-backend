import { createParamDecorator } from '@nestjs/common';

export const getUser = createParamDecorator((data, req) => {
  return data ? req.user[data] : req.user;
});
