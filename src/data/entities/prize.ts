import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { PrizeType } from '../../common/enums/prize.enum';
import { RedemptionRecord } from './redemptionRecord';

@Entity('prizes')
export class Prize {
  @PrimaryColumn()
  barcode: string;

  @Column()
  prize: PrizeType;

  @Column()
  count: number;
  @OneToMany(type => RedemptionRecord, record => record.prize)
  redemptionRecord: Promise<RedemptionRecord>;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @VersionColumn()
  version: number;
}
