import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Customer } from './customer';
import { RedemptionRecord } from './redemptionRecord';
import { User } from './user';

@Entity('outlets')
export class Outlet {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  location: string;

  @ManyToOne(type => Customer, customer => customer.outlets)
  brand: Promise<Customer>;

  @OneToMany(type => User, user => user.outlet)
  user: Promise<User[]>;

  @OneToMany(
    type => RedemptionRecord,
    redemptionRecord => redemptionRecord.user,
  )
  redemptionRecords: Promise<RedemptionRecord[]>;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @Column({ default: false })
  isDeleted: boolean;

  @VersionColumn()
  version: number;
}
