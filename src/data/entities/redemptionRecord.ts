import {
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Barcode } from './barcode';
import { Customer } from './customer';
import { Outlet } from './outlet';
import { Prize } from './prize';
import { User } from './user';

@Entity('redemptionRecords')
export class RedemptionRecord {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(type => Barcode, barcode => barcode.redemptionRecord, {
    eager: true,
  })
  @JoinColumn()
  barcode: Barcode;

  @ManyToOne(type => Customer, customer => customer.redemptionRecords)
  brand: Promise<Customer>;

  @ManyToOne(type => Outlet, outlet => outlet.redemptionRecords)
  outlet: Promise<Outlet>;

  @ManyToOne(type => User, user => user.redemptionRecords)
  user: Promise<User>;
  @ManyToOne(type => Prize, prize => prize.redemptionRecord, { eager: true })
  @JoinColumn()
  prize: Prize;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @VersionColumn()
  version: number;
}
