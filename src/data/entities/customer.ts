import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Outlet } from './outlet';
import { RedemptionRecord } from './redemptionRecord';
import { User } from './user';

@Entity('customers')
export class Customer {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  brandName: string;

  @OneToMany(type => Outlet, outlet => outlet.brand)
  outlets: Promise<Outlet[]>;

  @OneToMany(
    type => RedemptionRecord,
    redemptionRecord => redemptionRecord.brand,
  )
  redemptionRecords: Promise<RedemptionRecord[]>;

  @OneToMany(type => User, user => user.brand)
  users: Promise<User[]>;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @Column({ default: false })
  isDeleted: boolean;

  @VersionColumn()
  version: number;
}
