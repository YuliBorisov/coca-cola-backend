import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Customer } from './customer';
import { Outlet } from './outlet';
import { RedemptionRecord } from './redemptionRecord';
import { Role } from './role';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  name: string;

  @Column('nvarchar')
  password: string;

  @Column({ unique: true })
  email: string;

  @ManyToMany(type => Role, { eager: true })
  @JoinTable()
  roles: Role[];

  @ManyToOne(type => Outlet, outlet => outlet.user)
  outlet: Promise<Outlet>;

  @ManyToOne(type => Customer, customer => customer.users)
  brand: Promise<Customer>;

  @OneToMany(
    type => RedemptionRecord,
    redemptionRecord => redemptionRecord.user,
  )
  redemptionRecords: Promise<RedemptionRecord[]>;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @Column({ default: false })
  isDeleted: boolean;

  @VersionColumn()
  version: number;
}
