import {
  Column,
  CreateDateColumn,
  Entity,
  OneToOne,
  PrimaryColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { BarcodeStatus } from '../../models/barcode/barcode-status';
import { RedemptionRecord } from './redemptionRecord';

@Entity('barcodes')
export class Barcode {
  @PrimaryColumn()
  id: string;

  @OneToOne(
    type => RedemptionRecord,
    redemptionRecord => redemptionRecord.barcode,
  )
  redemptionRecord: Promise<RedemptionRecord>;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @VersionColumn()
  version: number;

  @Column({ default: BarcodeStatus.Valid })
  status: BarcodeStatus;
}
