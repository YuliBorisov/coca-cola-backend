import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ActivityType } from '../../common/enums/activity-type.enum';
import { FeatureType } from '../../common/enums/feature-type.enum';
import { MappingService } from '../../core/services/mapping.service';
import { Customer } from '../../data/entities/customer';
import { Outlet } from '../../data/entities/outlet';
import { User } from '../../data/entities/user';
import { CreateCustomerDTO } from '../../models/customer/create-customer-dto';
import { ActivityService } from '../activity/activity.service';

@Injectable()
export class CustomerService {
  public constructor(
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Outlet)
    private readonly outletRepository: Repository<Outlet>,
    private readonly activityService: ActivityService,
    private readonly mappingService: MappingService,
  ) {}

  async getAllCustomers() {
    const allCustomers = await this.customerRepository.find({
      where: {
        isDeleted: false,
      },
      order: {
        brandName: 'ASC',
      },
    });

    if (!allCustomers) {
      throw new NotFoundException(
        'There are no customers created yet, you need to create one first!',
      );
    }

    const mappedCustomers = await allCustomers.map(customer =>
      this.mappingService.returnCustomer(customer),
    );

    return await Promise.all(mappedCustomers);
  }

  async getCustomer(customerId: string) {
    const getCustomerById = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!getCustomerById) {
      throw new NotFoundException(
        'This customer was deleted! If there is an issue with this customer, contact our administrator for submitting this issue!',
      );
    }

    return await this.mappingService.returnCustomer(getCustomerById);
  }

  async createCustomer(customer: CreateCustomerDTO, user: User) {
    const checkExistingCustomer = await this.customerRepository.findOne({
      where: {
        brandName: customer.brandName,
      },
    });

    if (checkExistingCustomer && checkExistingCustomer.isDeleted === false) {
      throw new BadRequestException('Customer already exists');
    }
    if (checkExistingCustomer) {
      checkExistingCustomer.isDeleted = false;
      const editedIsDeletedCustomer = await this.customerRepository.save(
        checkExistingCustomer,
      );
      return await this.mappingService.returnCustomer(editedIsDeletedCustomer);
    }

    const newCustomer = this.customerRepository.create();
    newCustomer.brandName = customer.brandName;

    const saveCustomer = await this.customerRepository.save(newCustomer);

    if (saveCustomer && Object.keys(saveCustomer).length !== 0) {
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.customer,
        ActivityType.CREATED,
        newCustomer.id,
        newCustomer.brandName,
      );
    }

    return await this.mappingService.returnCustomer(saveCustomer);
  }

  async updateCustomer(
    customerId: string,
    customer: CreateCustomerDTO,
    user: User,
  ) {
    const oldCustomer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!oldCustomer) {
      throw new NotFoundException(
        'This customer was deleted! If there is an issue with this customer, contact our administrator for submitting this issue!',
      );
    }

    const customerToUpdate = { ...oldCustomer, ...customer };

    const updatedCustomer = await this.customerRepository.save(
      customerToUpdate,
    );

    if (updatedCustomer && Object.keys(updatedCustomer).length !== 0) {
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.customer,
        ActivityType.UPDATED,
        oldCustomer.id,
        oldCustomer.brandName,
      );
    }

    return await updatedCustomer;
  }

  async deleteCustomer(customerId: string, user: User) {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });
    if (!customer) {
      throw new BadRequestException('This customer does not exist!');
    }

    customer.isDeleted = true;
    const deletedCustomer = await this.customerRepository.save(customer);

    if (deletedCustomer && Object.keys(deletedCustomer).length !== 0) {
      const customerOutlets = await this.outletRepository.find({
        where: {
          brand: deletedCustomer.id,
          isDeleted: false,
        },
      });
      customerOutlets.map(async outlet => {
        const outletUsers = await this.userRepository.find({
          where: {
            outlet,
            isDeleted: false,
          },
        });
        outletUsers.map(async outletUser => {
          outletUser.isDeleted = true;
          await this.userRepository.save(outletUser);
        });
        outlet.isDeleted = true;
        await this.outletRepository.save(outlet);
      });
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.customer,
        ActivityType.DELETED,
        customer.id,
        customer.brandName,
      );
    }

    return await deletedCustomer;
  }
}
