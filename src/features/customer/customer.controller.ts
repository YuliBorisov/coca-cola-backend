import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../../common/decorators/roles.decorator';
import { getUser } from '../../common/decorators/user.decorator';
import { UserRole } from '../../common/enums/user-role.enum';
import { ControllerRolesGuard } from '../../common/guards/controller.roles.guard';
import { User } from '../../data/entities/user';
import { CreateCustomerDTO } from '../../models/customer/create-customer-dto';
import { CustomerService } from './customer.service';

@Controller()
@UseGuards(AuthGuard(), ControllerRolesGuard)
@Roles(UserRole.Admin)
export class CustomerController {
  public constructor(private readonly customerService: CustomerService) {}

  @Get('customer/:id')
  @HttpCode(HttpStatus.OK)
  async getCustomer(@Param('id') id: string) {
    return await this.customerService.getCustomer(id);
  }

  @Get('customers')
  @HttpCode(HttpStatus.OK)
  async getAllCustomers() {
    return await this.customerService.getAllCustomers();
  }

  @Post('customer')
  @HttpCode(HttpStatus.CREATED)
  async createCustomer(
    @getUser() user: User,
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    customer: CreateCustomerDTO,
  ) {
    return await this.customerService.createCustomer(customer, user);
  }

  @Put('customer/:customerId')
  @HttpCode(HttpStatus.OK)
  async updateCustomer(
    @Param('customerId') id: string,
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    customer: CreateCustomerDTO,
    @getUser() user: User,
  ) {
    return await this.customerService.updateCustomer(id, customer, user);
  }

  @Delete('customer/:customerId')
  @HttpCode(HttpStatus.OK)
  async deleteCustomer(@Param('customerId') id: string, @getUser() user: User) {
    return await this.customerService.deleteCustomer(id, user);
  }
}
