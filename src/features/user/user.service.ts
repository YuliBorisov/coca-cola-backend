import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { ActivityType } from '../../common/enums/activity-type.enum';
import { FeatureType } from '../../common/enums/feature-type.enum';
import { UserRole } from '../../common/enums/user-role.enum';
import { MailerService } from '../../core/services/mailer.service';
import { MappingService } from '../../core/services/mapping.service';
import { Customer } from '../../data/entities/customer';
import { Outlet } from '../../data/entities/outlet';
import { Role } from '../../data/entities/role';
import { User } from '../../data/entities/user';
import { AdminRegisterDTO } from '../../models/user/admin-register-dto';
import { UserRegisterDTO } from '../../models/user/user-register-dto';
import { ActivityService } from '../activity/activity.service';

@Injectable()
export class UserService {
  public constructor(
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
    @InjectRepository(Outlet)
    private readonly outletRepository: Repository<Outlet>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Role) private readonly roleRepository: Repository<Role>,
    private readonly activityService: ActivityService,
    private readonly mappingService: MappingService,
    private readonly mailerService: MailerService,
  ) {}

  async getAllUsersForCurrentOutlet(customerId: string, outletId: string) {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!customer) {
      throw new NotFoundException(
        'There is no such customer! Please try another criteria!',
      );
    }

    const outlet = await this.outletRepository.findOne({
      where: {
        id: outletId,
        customer,
        isDeleted: false,
      },
    });

    if (!outlet) {
      throw new NotFoundException(
        'There is no such outlet! Please try another criteria!',
      );
    }

    const users = await this.userRepository.find({
      relations: ['brand', 'outlet'],
      where: {
        outlet,
        isDeleted: false,
      },

      order: {
        name: 'ASC',
      },
    });

    if (users.length === 0) {
      throw new NotFoundException(
        'There are no users yet in this outlet! Try creating one first!',
      );
    }

    const mappedUsers = await users.map(user =>
      this.mappingService.returnBasicUser(user),
    );

    return await Promise.all(mappedUsers);
  }

  async getAllBasicUsers() {
    const getAllUsers = await this.userRepository.find({
      where: {
        isDeleted: false,
      },
      relations: ['brand', 'outlet'],
    });

    const allBasicUsers = await getAllUsers
      .filter(user => user.roles.map(role => role.name).includes('Basic'))
      .map(allUsers => this.mappingService.returnBasicUser(allUsers));

    return await Promise.all(allBasicUsers);
  }

  async getAllAdmins() {
    const allUsers = await this.userRepository.find({
      where: {
        isDeleted: false,
      },
      order: {
        name: 'ASC',
      },
    });

    // const allAdmins = await allUsers.map((x) => {
    //   return {
    //     id: x.id,
    //     userName: x.name,
    //     createdOn: x.createdOn,
    //     roles: x.roles.map(role => role.name),
    //   };
    // }).filter(user => user.roles.includes('Admin'));

    const allAdminUsers = await allUsers
      .filter(user => user.roles.map(role => role.name).includes('Admin'))
      .map(admin => this.mappingService.returnUser(admin));

    return await Promise.all(allAdminUsers);
  }

  async getCurrentUser(userId: string) {
    const currentUser = await this.userRepository.findOne({
      where: {
        id: userId,
        isDeleted: false,
      },
    });

    await currentUser.brand;
    await currentUser.outlet;

    return await await this.mappingService.returnBasicUser(currentUser);
  }

  async registerUser(
    customerId: string,
    outletId: string,
    userRegister: UserRegisterDTO,
    user: User,
  ) {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!customer) {
      throw new NotFoundException(
        'There is no such customer! Please try another criteria!',
      );
    }

    const outlet = await this.outletRepository.findOne({
      where: {
        id: outletId,
        brand: customer.id,
        isDeleted: false,
      },
    });

    if (!outlet) {
      throw new NotFoundException(
        'There is no such outlet! Please try another criteria!',
      );
    }

    userRegister.password = await bcrypt.hash(userRegister.password, 10);

    const basicRole = await this.roleRepository.findOne({
      name: UserRole.Basic,
    });

    const newUser = new User();
    newUser.name = userRegister.name;
    newUser.email = userRegister.email;
    newUser.password = userRegister.password;
    newUser.brand = Promise.resolve(customer);
    newUser.outlet = Promise.resolve(outlet);
    newUser.roles = [basicRole];

    const savedUser = await this.userRepository.save(newUser);

    if (savedUser && Object.keys(savedUser).length !== 0) {
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.user,
        ActivityType.CREATED,
        newUser.id,
        newUser.name,
      );
    }

    return await this.mappingService.returnBasicUser(savedUser);
  }

  async registerAdmin(admin: AdminRegisterDTO, user: User) {
    admin.password = await bcrypt.hash(admin.password, 10);

    const adminRole = await this.roleRepository.findOne({
      name: UserRole.Admin,
    });

    const newAdmin = new User();
    newAdmin.name = admin.name;
    newAdmin.email = admin.email;
    newAdmin.password = admin.password;
    newAdmin.roles = [adminRole];

    const saveNewAdmin = await this.userRepository.save(newAdmin);

    if (saveNewAdmin && Object.keys(saveNewAdmin).length !== 0) {
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.user,
        ActivityType.CREATED,
        newAdmin.id,
        newAdmin.name,
      );
    }

    const newAdminToReturn = {
      id: saveNewAdmin.id,
      userName: saveNewAdmin.name,
      createdOn: saveNewAdmin.createdOn,
      roles: saveNewAdmin.roles.map(role => role.name),
    };

    return await newAdminToReturn;
  }

  async editUser(
    customerId: string,
    outletId: string,
    userId: string,
    newOutletId: string,
    user: User,
  ) {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!customer) {
      throw new NotFoundException(
        'There is no such customer! Please try another criteria!',
      );
    }

    const currentOutlet = await this.outletRepository.findOne({
      where: {
        id: outletId,
        customer,
        isDeleted: false,
      },
    });

    if (!currentOutlet) {
      throw new NotFoundException(
        'There is no such outlet! Please try another criteria!',
      );
    }

    const foundUser = await this.userRepository.findOne({
      where: {
        currentOutlet,
        customer,
        id: userId,
        isDeleted: false,
      },
    });

    if (!foundUser) {
      throw new NotFoundException(
        'There is no such user! Please try another criteria!',
      );
    }

    const newOutlet = await this.outletRepository.findOne({
      where: {
        id: newOutletId,
        isDeleted: false,
      },
    });

    foundUser.outlet = Promise.resolve(newOutlet);

    const updatedUser = await this.userRepository.save(foundUser);

    if (updatedUser && Object.keys(updatedUser).length !== 0) {
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.user,
        ActivityType.UPDATED,
        foundUser.id,
        foundUser.name,
      );
    }

    return await this.mappingService.returnBasicUser(updatedUser);
  }

  async deleteUser(
    customerId: string,
    outletId: string,
    userId: string,
    user: User,
  ) {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!customer) {
      throw new NotFoundException(
        'There is no such customer! Please try another criteria!',
      );
    }

    const outlet = await this.outletRepository.findOne({
      where: {
        id: outletId,
        customer,
        isDeleted: false,
      },
    });

    if (!outlet) {
      throw new NotFoundException(
        'There is no such outlet! Please try another criteria!',
      );
    }

    const userToDelete = await this.userRepository.findOne({
      where: {
        outlet,
        customer,
        id: userId,
        isDeleted: false,
      },
    });

    if (!userToDelete) {
      throw new NotFoundException(
        'There is no such user! Please try another criteria!',
      );
    }

    userToDelete.isDeleted = true;

    const deletedUser = await this.userRepository.save(userToDelete);

    if (deletedUser && Object.keys(deletedUser).length !== 0) {
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.user,
        ActivityType.DELETED,
        userToDelete.id,
        userToDelete.name,
      );
    }

    return await deletedUser;
  }

  async deleteAdmin(adminId: string, user: User) {
    const adminToDelete = await this.userRepository.findOne({
      where: {
        id: adminId,
      },
    });

    if (!adminToDelete) {
      throw new NotFoundException(
        'There is no such user! Please try another criteria!',
      );
    }

    adminToDelete.isDeleted = true;

    const deletedAdmin = await this.userRepository.save(adminToDelete);

    if (deletedAdmin && Object.keys(deletedAdmin).length !== 0) {
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.user,
        ActivityType.DELETED,
        adminToDelete.id,
        adminToDelete.name,
      );
    }
  }

  async submitIssue(issueObj: { name: string; email: string; issue: string }) {
    return this.mailerService.submitIssue(issueObj);
  }
}
