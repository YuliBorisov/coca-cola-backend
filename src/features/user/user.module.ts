import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { CoreModule } from '../../core/core.module';
import { AuthModule } from '../../auth/auth.module';
import { ActivityModule } from '../activity/activity.module';
import { MappingService } from '../../core/services/mapping.service';

@Module({
  controllers: [UserController],
  providers: [UserService, MappingService],
  imports: [CoreModule, AuthModule, ActivityModule],
})
export class UserModule {}
