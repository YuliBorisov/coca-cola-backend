import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../../common/decorators/roles.decorator';
import { getUser } from '../../common/decorators/user.decorator';
import { UserRole } from '../../common/enums/user-role.enum';
import { ControllerRolesGuard } from '../../common/guards/controller.roles.guard';
import { User } from '../../data/entities/user';
import { AdminRegisterDTO } from '../../models/user/admin-register-dto';
import { UserMappingDTO } from '../../models/user/user-mapping-dto';
import { UserRegisterDTO } from '../../models/user/user-register-dto';
import { UserService } from './user.service';

@Controller()
export class UserController {
  public constructor(private readonly userService: UserService) {}

  @Get('customer/:customerId/outlet/:outletId/users')
  @UseGuards(AuthGuard(), ControllerRolesGuard)
  @Roles(UserRole.Admin)
  @HttpCode(HttpStatus.OK)
  async getAllUsersForCurrentOutlet(
    @Param('customerId') customerId: string,
    @Param('outletId') outletId: string,
  ) {
    return await this.userService.getAllUsersForCurrentOutlet(
      customerId,
      outletId,
    );
  }

  @Get('users')
  @UseGuards(AuthGuard(), ControllerRolesGuard)
  @Roles(UserRole.Admin)
  @HttpCode(HttpStatus.OK)
  async getAllAdmins() {
    return await this.userService.getAllAdmins();
  }

  @Get('basic-user')
  @UseGuards(AuthGuard())
  @HttpCode(HttpStatus.OK)
  async getAllUsers() {
    return await this.userService.getAllBasicUsers();
  }

  @Get('user/:userId')
  @UseGuards(AuthGuard())
  // @Roles(UserRole.Admin)
  @HttpCode(HttpStatus.OK)
  async getCurrentUser(@Param('userId') userId: string) {
    return await this.userService.getCurrentUser(userId);
  }

  @Post('customer/:customerId/outlet/:outletId/user')
  @UseGuards(AuthGuard(), ControllerRolesGuard)
  @Roles(UserRole.Admin)
  @HttpCode(HttpStatus.CREATED)
  async registerUser(
    @Param('customerId') customerId: string,
    @Param('outletId') outletId: string,
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    userRegister: UserRegisterDTO,
    @getUser() user: User,
  ) {
    return await this.userService.registerUser(
      customerId,
      outletId,
      userRegister,
      user,
    );
  }

  @Post('user')
  @UseGuards(AuthGuard(), ControllerRolesGuard)
  @Roles(UserRole.Admin)
  @HttpCode(HttpStatus.CREATED)
  async registerAdmin(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    admin: AdminRegisterDTO,
    @getUser() user: User,
  ) {
    return await this.userService.registerAdmin(admin, user);
  }

  @Put('customer/:customerId/outlet/:outletId/user/:userId')
  @UseGuards(AuthGuard(), ControllerRolesGuard)
  @Roles(UserRole.Admin)
  @HttpCode(HttpStatus.OK)
  async editUser(
    @Param('customerId') customerId: string,
    @Param('outletId') outletId: string,
    @Param('userId') userId: string,
    @Body() newOutlet: UserMappingDTO,
    @getUser() user: User,
  ) {
    return await this.userService.editUser(
      customerId,
      outletId,
      userId,
      newOutlet.outletId,
      user,
    );
  }

  @Delete('customer/:customerId/outlet/:outletId/user/:userId')
  @UseGuards(AuthGuard(), ControllerRolesGuard)
  @Roles(UserRole.Admin)
  @HttpCode(HttpStatus.OK)
  async deleteUser(
    @Param('customerId') customerId: string,
    @Param('outletId') outletId: string,
    @Param('userId') userId: string,
    @getUser() user: User,
  ) {
    return await this.userService.deleteUser(
      customerId,
      outletId,
      userId,
      user,
    );
  }

  @Delete('user/:adminId')
  @UseGuards(AuthGuard(), ControllerRolesGuard)
  @Roles(UserRole.Admin)
  @HttpCode(HttpStatus.OK)
  async deleteAdmin(@Param('adminId') adminId: string, @getUser() user: User) {
    return await this.userService.deleteAdmin(adminId, user);
  }

  @Post('submit-issue')
  @UseGuards(AuthGuard())
  @HttpCode(HttpStatus.OK)
  async sumbitIssue(@Body()
  issueObj: {
    name: string;
    email: string;
    issue: string;
  }) {
    return await this.userService.submitIssue(issueObj);
  }
}
