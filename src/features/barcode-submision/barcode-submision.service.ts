import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MailerService } from '../../core/services/mailer.service';
import { Barcode } from '../../data/entities/barcode';
import { Prize } from '../../data/entities/prize';
import { RedemptionRecord } from '../../data/entities/redemptionRecord';
import { User } from '../../data/entities/user';
import { BarcodeDTO } from '../../models/barcode/barcode-dto';
import { BarcodeStatus } from '../../models/barcode/barcode-status';
import { PrizeBarcodeDTO } from '../../models/prizeRedemption/prize-barcode-dto';

@Injectable()
export class BarcodeSubmisionService {
  public constructor(
    @InjectRepository(RedemptionRecord)
    private readonly redemptionRecordRepository: Repository<RedemptionRecord>,
    @InjectRepository(Barcode)
    private readonly barcodeRepository: Repository<Barcode>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Prize)
    private readonly prizeRepository: Repository<Prize>,
    private readonly mailerService: MailerService,
  ) {}

  async submitBarcode(barcode: BarcodeDTO, user: User) {
    // TODO validate the barcode and decide whether to create another service for validation

    const winningBarcodeCheck = await this.barcodeRepository.findOne({
      where: { id: barcode.barcode },
    });

    if (!winningBarcodeCheck) {
      return { message: 'Invalid!' };
    }

    if (winningBarcodeCheck.status === BarcodeStatus.AlreadyRedeemed) {
      return { message: 'Already redeemed' };
    }

    return { message: 'Valid!' };
  }

  async redeemPrize(barcodes: PrizeBarcodeDTO, user: User) {
    const barcodeForRedemption = await this.barcodeRepository.findOne({
      where: { id: barcodes.barcode },
    });

    if (!barcodeForRedemption) {
      throw new BadRequestException({ message: 'Not a winning barcode!' });
    }

    if (barcodeForRedemption.status === BarcodeStatus.AlreadyRedeemed) {
      throw new BadRequestException({ message: 'Already Redeemed' });
    }

    const prizeForRedemption = await this.prizeRepository.findOne({
      where: { barcode: barcodes.prizeBarcode },
    });

    if (!prizeForRedemption) {
      throw new BadRequestException({ message: 'Not a valid prize barcode!' });
    }

    if (prizeForRedemption.count === 0) {
      throw new BadRequestException({
        message: 'No available inventory for the selected promotional prize!',
      });
    }

    if (barcodeForRedemption && prizeForRedemption) {
      barcodeForRedemption.status = BarcodeStatus.AlreadyRedeemed;
      await this.barcodeRepository.save(barcodeForRedemption);
      const foundUser = await this.userRepository.findOne(user.id);
      await foundUser.brand;
      await foundUser.outlet;

      const record = new RedemptionRecord();
      record.barcode = await barcodeForRedemption;
      record.brand = Promise.resolve(foundUser.brand);
      record.outlet = Promise.resolve(foundUser.outlet);
      record.user = Promise.resolve(foundUser);
      record.prize = await prizeForRedemption;

      prizeForRedemption.count -= 1;
      await this.prizeRepository.save(prizeForRedemption);

      return await this.redemptionRecordRepository.save(record);
    }
    return null;
  }

  async reportMultipleRedemption(barcode: BarcodeDTO) {
    return await this.mailerService.reportMultipleRedemption(barcode.barcode);
  }
}
