import { Test, TestingModule } from '@nestjs/testing';
import { BarcodeSubmisionService } from './barcode-submision.service';

describe('BarcodeSubmisionService', () => {
  let service: BarcodeSubmisionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BarcodeSubmisionService],
    }).compile();

    service = module.get<BarcodeSubmisionService>(BarcodeSubmisionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
