import { Test, TestingModule } from '@nestjs/testing';
import { RedemptionReportController } from './redemption-report.controller';

describe('RedemptionReport Controller', () => {
  let controller: RedemptionReportController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RedemptionReportController],
    }).compile();

    controller = module.get<RedemptionReportController>(RedemptionReportController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
