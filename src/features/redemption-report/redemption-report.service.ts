import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MappingService } from '../../core/services/mapping.service';
import { Activity } from '../../data/entities/activity';
import { RedemptionRecord } from '../../data/entities/redemptionRecord';
import { User } from '../../data/entities/user';
import { ShowRedemptionRecordDTO } from '../../models/redemption-record/redemption-record-dto';

@Injectable()
export class RedemptionReportService {
  public constructor(
    @InjectRepository(RedemptionRecord)
    private readonly redemptionRecordRepository: Repository<RedemptionRecord>,
    @InjectRepository(Activity)
    private readonly activityRepository: Repository<Activity>,
    private readonly mappingService: MappingService,
  ) {}

  async getRedemptionRecords(): Promise<ShowRedemptionRecordDTO[]> {
    const records = await this.redemptionRecordRepository.find({
      relations: ['user', 'brand', 'outlet'],
    });

    return await Promise.all(
      records.map(record => this.mappingService.returnRedemptionRecord(record)),
    );
  }

  async getUserRedemptionRecords(
    user: User,
  ): Promise<ShowRedemptionRecordDTO[]> {
    const records = await this.redemptionRecordRepository.find({
      where: {
        user,
      },
      relations: ['user', 'brand', 'outlet'],
    });

    return await Promise.all(
      records.map(record => this.mappingService.returnRedemptionRecord(record)),
    );
  }

  async getOutletRedemptionRecords(
    user: User,
  ): Promise<ShowRedemptionRecordDTO[]> {
    const outlet = await user.outlet;

    const records = await this.redemptionRecordRepository.find({
      where: {
        outlet,
      },
      relations: ['user', 'brand', 'outlet'],
    });

    return await Promise.all(
      records.map(record => this.mappingService.returnRedemptionRecord(record)),
    );
  }

  async getActivity(): Promise<Activity[]> {
    return this.activityRepository.find();
  }
}
