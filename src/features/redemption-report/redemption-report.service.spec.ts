import { Test, TestingModule } from '@nestjs/testing';
import { RedemptionReportService } from './redemption-report.service';

describe('RedemptionReportService', () => {
  let service: RedemptionReportService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RedemptionReportService],
    }).compile();

    service = module.get<RedemptionReportService>(RedemptionReportService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
