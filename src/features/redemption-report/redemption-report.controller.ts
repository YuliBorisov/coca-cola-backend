import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../../common/decorators/roles.decorator';
import { getUser } from '../../common/decorators/user.decorator';
import { UserRole } from '../../common/enums/user-role.enum';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Activity } from '../../data/entities/activity';
import { User } from '../../data/entities/user';
import { ShowRedemptionRecordDTO } from '../../models/redemption-record/redemption-record-dto';
import { RedemptionReportService } from './redemption-report.service';

@Controller()
export class RedemptionReportController {
  public constructor(
    private readonly redemptionReportService: RedemptionReportService,
  ) {}
  @Get('redemption-record')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async getRedemptionRecords(): Promise<ShowRedemptionRecordDTO[]> {
    return await this.redemptionReportService.getRedemptionRecords();
  }

  @Get('user-record')
  @UseGuards(AuthGuard())
  async getUserRedemptionRecords(
    @getUser() user: User,
  ): Promise<ShowRedemptionRecordDTO[]> {
    return await this.redemptionReportService.getUserRedemptionRecords(user);
  }

  @Get('outlet-record')
  @UseGuards(AuthGuard())
  async getOutletRedemptionRecords(
    @getUser() user: User,
  ): Promise<ShowRedemptionRecordDTO[]> {
    return await this.redemptionReportService.getOutletRedemptionRecords(user);
  }

  @Get('activity-log')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async getActivity(): Promise<Activity[]> {
    return await this.redemptionReportService.getActivity();
  }
}
