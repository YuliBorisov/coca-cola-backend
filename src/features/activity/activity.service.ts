import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ActivityType } from '../../common/enums/activity-type.enum';
import { FeatureType } from '../../common/enums/feature-type.enum';
import { Activity } from '../../data/entities/activity';

@Injectable()
export class ActivityService {
  public constructor(
    @InjectRepository(Activity)
    private readonly activityRepository: Repository<Activity>,
  ) { }

  async createActivityRecord(
    modifyingUserId: string,
    modifyingUserName: string,
    featureType: FeatureType,
    actionType: ActivityType,
    itemId: string,
    itemName: string,
  ): Promise<Activity> {
    const activityRecord = new Activity();
    activityRecord.modifyingUserId = modifyingUserId;
    activityRecord.modifyingUserName = modifyingUserName;
    activityRecord.featureType = featureType;
    activityRecord.actionType = actionType;
    activityRecord.itemId = itemId;
    activityRecord.itemName = itemName;

    return this.activityRepository.save(activityRecord);
  }

  async getActivityRecord() {

    const allActivity = await this.activityRepository.find();

    const activity = await allActivity.map(x => {
      return {
        modifyingUserName: x.itemName,
        featureType: x.featureType,
        actionType: x.actionType,
        itemName: x.itemName,
        createdOn: x.createdOn,
        updatedOn: x.updatedOn,
      };
    });

    if (activity.length === 0) {
      throw new NotFoundException('There is no activity yet!');
    }

    return await activity;
  }
}
