import { Module } from '@nestjs/common';
import { CoreModule } from '../../core/core.module';
import { ActivityController } from './activity.controller';
import { ActivityService } from './activity.service';
import { AuthModule } from '../../auth/auth.module';

@Module({
  controllers: [ActivityController],
  providers: [ActivityService],
  imports: [CoreModule, AuthModule],
  exports: [ActivityService],
})
export class ActivityModule {}
