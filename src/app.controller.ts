import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from './common/decorators/roles.decorator';
import { getUser } from './common/decorators/user.decorator';
import { UserRole } from './common/enums/user-role.enum';
import { RolesGuard } from './common/guards/roles.guard';

@Controller()
export class AppController {
  @Get()
  @UseGuards(AuthGuard())
  root(@getUser() authenticatedUser) {

    return {
      data: `Yay, you're logged in!`,
    };
  }

  @Get('/admin')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  admin(@getUser() authenticatedUser) {

    return {
      data: `Yay, you are an admin!`,
    };
  }
}
