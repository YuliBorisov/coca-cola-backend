# Prize Redemption Application project Build with NestJS

Steps to run this project:

1. Run `npm i` command
2. Setup .env file with the following: `PORT, JWT_SECRET, DB_TYPE, DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_DAABASE_NAME, DB_DATABASE_ADMIN_PASS`;
3. Setup database settings inside `ormconfig.json` file
4. Run `npm run start:dev` command
5. For initial NestCLI documentation, run `npx compodoc -p tsconfig.json -s` or `npx compodoc -s` and open your browser and navigate to `http://127.0.0.1:8080`;
